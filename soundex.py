
#!/usr/bin/env python3

import sys
import re


def soundex(word):

    letters1 = "BbFfPpVv"
    letters2 = "CcGgJjKkQqSsXxZz"
    letters3 = "DdTt"
    letters4 = "Ll"
    letters5 = "MmNn"
    letters6 = "Rr"
    allletters = letters1 + letters2 + letters3 + letters4 + letters5 + letters6

    res = word[0].capitalize() + "-"        # 1. Buchstabe, Groß
    restletters = word[1:]                  # weitere Buchstaben
    digitletters = ""

    for char in restletters:                # relevante Buchstaben rausfiltern
        if char in allletters:
            digitletters += char

    while len(digitletters) < 3:
        digitletters += "0"                 # immer 3 'char' zum übersetzen

    for  char in digitletters[0:3]:         # 1. 3 relevante Buchstaben übersetzen
        if char in letters1:
            res += "1"
        if char in letters2:
            res += "2"
        if char in letters3:
            res += "3"
        if char in letters4:
            res += "4"
        if char in letters5:
            res += "5"
        if char in letters6:
            res += "6"
        if char not in allletters:
            res += "0"

    return res

""" Der Test hat noch einen Fehler gefunden, aber die entsteht
durch eine falsche Übersetzung im Test: Amanda = AMND = A-553"""

def main(args):

    words = args
    for word in words:
        print(soundex(word))


if __name__ == "__main__":
    args = sys.argv[1:]
    main(args)
