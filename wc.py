#!/usr/bin/env python3


import sys
import re
from tokenizer import tokenize


def main(args):
    arguments = args[0:-1]
    ifile = args[-1]

    res = ""

    for elem in arguments:
        if elem == '-l':                    # line count
            with open(ifile, "r", encoding="utf-8") as infile:
                ctr = 0
                for line in infile:
                    ctr += 1
                res += str(ctr) + " "
        elif elem == '-c':                  # character count
            with open(ifile, "r", encoding="utf-8") as infile:
                ctr = 0
                for line in infile:
                    line = line.strip()
                    ctr += len(line)
                res += str(ctr) + " "
        elif elem == '-w':                  # word count
            with open(ifile, "r", encoding="utf-8") as infile:
                ctr = 0
                for line in infile:
                    line = line.strip()
                    word = tokenize(line)
                    ctr += len(word)
                res += str(ctr) + " "
    res += ifile
    print(res)

""" Der Test hat noch zwei Fehler gefunden, aber die entstehen
durch eine Verwechslung von c und w im Test"""

if __name__ == "__main__":

    args = sys.argv[1:]
    main(args)
