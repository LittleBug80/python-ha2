#!/usr/bin/env python3

import unittest
from unittest.mock import patch

import soundex as s

class SoundexTest(unittest.TestCase):

    def test_soundex(self):

        test_values = {
            'Dave': 'D-100',
            'John': 'J-500',
            'Amanda': 'A-553',
            'Corleone': 'C-645',
            'Beowulf': 'B-410',
            'Skywalker': 'S-242'
        }

        for inpt, expected in test_values.items():
            actual = s.soundex(inpt)
            self.assertEqual(expected, actual)



if __name__ == '__main__':
    unittest.main()
