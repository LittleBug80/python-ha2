#!/usr/bin/env python3

import unittest
from unittest.mock import patch

import tokenizer as t

class TokenizeTest(unittest.TestCase):

    def test_tokenize_empty(self):

        test_string = ""

        actual_tokens = t.tokenize(test_string)
        expected_tokens = []

        self.assertEqual(actual_tokens, expected_tokens)

    def test_tokenize(self):

        test_string = "I'm a happy little text. You're 2. place. He's first!"

        actual_tokens = t.tokenize(test_string)
        expected_tokens = ['I', "'m", 'a', 'happy', 'little', 'text', '.', 'You', "'re", '2', '.', 'place', '.', 'He', "'s", 'first', '!']

        self.assertEqual(actual_tokens, expected_tokens)

if __name__ == '__main__':
    unittest.main()
