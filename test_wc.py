#!/usr/bin/env python3

import unittest
from unittest.mock import patch

import wc

class WCTest(unittest.TestCase):

    @patch("builtins.print")
    def test_wc_all(self, mock_print):

        args = "-l -w -c test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("1 17 53 test.txt")

    @patch("builtins.print")
    def test_wc_line(self, mock_print):

        args = "-l test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("1 test.txt")

    @patch("builtins.print")
    def test_wc_words(self, mock_print):

        args = "-w test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("17 test.txt")

    @patch("builtins.print")
    def test_wc_chars(self, mock_print):

        args = "-c test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("53 test.txt")

    @patch("builtins.print")
    def test_wc_chars_lines(self, mock_print):

        args = "-l -c test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("1 53 test.txt")

    @patch("builtins.print")
    def test_wc_chars_words(self, mock_print):

        args = "-w -c test.txt".split()

        wc.main(args)
        mock_print.assert_called_with("17 53 test.txt")

if __name__ == '__main__':
    unittest.main()
