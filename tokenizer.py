#!/usr/bin/env python3


import sys
import re


def tokenize(string):
    res = ""

    for char in string:
        if char not in ".:,;!'?":
            res += char
        else:
            res += " " + char

    res = res.split()
    return res


def main(args):

    filename = args[0]
    with open(filename) as f:
        for line in f:
            print(tokenize(line))

if __name__ == "__main__":
    args = sys.argv[1:]
    main(args)
